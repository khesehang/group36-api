const fs = require('fs');
function myWrite(filename, content, cb) {
    fs.writeFile('./files/' + filename, content, function (err, done) {
        if (err) {
            //
            cb(err)
        } else {
            // 
            cb(null, done)
        }
    })
}
function myWriteA(filename, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('./files/' + filename, content, function (err, done) {
            if (err) {
                //
                reject(err)
            } else {
                //
                resolve(done)
            }
        })
    })

}

module.exports = {
    a: myWrite,
    b: myWriteA
}
