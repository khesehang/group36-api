// NODE ==> JS run time environment
// server side JS
// database communication

// command
// node --version
// npm --version
// npx --version

// npm and npx pacakage management tool
// npm
// yarn simlar to npm

// npx TODO discuss when starting react

// npm --init

// intilize js project (regardless of application area)

// it will help to create package.json
// pacakge.json file hold overall information about project
// try to keep updated information inside pacakges.json

// package-lock.json ==> this file will lock the version of dependent pacakges
// installation version lock

// npmjs.com npm registry ===> container that holds javascript projects
// npm install <packge.name>
// populate pacakge.json file >> package-lock.json, node_modules folder
// node_modules folder where installed packages are kept


// npm install ==> checks dependency section of package.json and install all the dependecies listed there



// to import all the modules from node_modules  folder and nodejs internal module
// don't give path
// eg const events = require('events')

// give path for your own files
// const a = require('./myFile')

// web architecture
// MVC
// separation of concerns 
// models
// views 
// controller

// 3 tier architecture
// presentation layer (views)
// application layer (controller)
// data layer (models)

// Client-server architecture
// ==> communication between two programme
// protocol must be same for communication
// request pathaune programme is Client
// request accept garne ,process garne and response programme is server


// research topic
// architecture vs design pattern



// http protocol
// http verb (methods)
// GET,POST,PUT,DELETE,PATCH
// http url 

// request object
// response object

// http status code
// 100,
// 200, ==> success
// 300,
// 400, ==> application error 
// 500,===> server error 
