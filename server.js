// node js internal module ---> http

const http = require('http');
const fileOp = require('./abc');

// browser ko url hit garda jahile pani GET request jancha
const server = http.createServer(function (request, response) {
    console.log('http client connected to server')
    console.log('http method >>', request.method);
    console.log('http url >>', request.url);
    // var aa = '/write/admin.txt/i am admin'
    // hint action==> aa.split('/')
    if (request.url === '/write') {
        // write operation
        fileOp
            .b('broadway.txt', 'broadway infosys nepal')
            .then(function (data) {
                response.end('file writing successfull', data)
            })
            .catch(function (err) {
                response.end('file writing failed >>', err)
            })
    }
    else if (request.url === '/read') {
        // read operation
    }
    else {
        response.end('no any action to perform')
    }
    // req-res cycle must be completed 
    // response.end('Hello from node server')
    // request or 1st arg placeholder is http request object
    // response or 2nd arg placeholder is http response object
    // callback
    // this callback will be executed once client sends requests
    // for http communication 
    // url and method must be there
    // endpoint ==> combination of http method and url
    // regardless of http method and regardless of http url this callback will be executed
})
// server is programme
// programme vs process
// programme is set of instructions

server.listen(9000, '127.0.0.1', function (err, done) {
    if (err) {
        console.log('server listening failed', err);
    } else {
        console.log('server listening at port 9000')
        console.log('press CTRL + C to exit')
    }
})



// express web application framework
// views

// object === javascript
// callback === nodejs

// middleware === express
