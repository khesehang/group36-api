// import internal module for file CRUD
const fs = require('fs');
const fileOperation = require('./abc.js');
console.log('file operation >>', fileOperation)
// write 
// fs.writeFile('./files/kishor.txt', 'welcome back to nodejs', function (err, done) {
//     if (err) {
//         console.log('file writing failed', err);
//     }
//     else {
//         console.log('file writing success', done)
//     }
// })

// read
// fs.readFile('./files/broadway.txt', 'UTF-8', function (err, done) {
//     if (err) {
//         console.log('error reading >>', err);
//     } else {
//         console.log('success in reading >>', done)
//     }
// })
// task ===>
// make it functional
// try using promise as well as callback
// seperate task and execution and run execution file


// task ===>
// make it functional
// try using promise as well as callback
// seperate task and execution and run execution file

// remove
fs.unlink('./files/broadway.txt', function (err, done) {
    if (err) {
        console.log('error in remvoing', err);
    } else {
        console.log('success in removing', done)
    }
})
// task ===>
// make it functional
// try using promise as well as callback
// seperate task and execution and run execution file


// rename
fs.rename('./files/broadway.txt', './files/node.js', function (err, renamed) {
    if (err) {
        console.log('error in ranaming>>', err);
    }
    else {
        console.log('success in renameing >>', renamed)
    }
})


// make it functional
// callback
// function myWrite(filename, content, cb) {
//     fs.writeFile('./files/' + filename, content, function (err, done) {
//         if (err) {
//             //
//             cb(err)
//         } else {
//             // 
//             cb(null, done)
//         }
//     })
// }

// promise
function myWrite(filename, content) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('./files/' + filename, content, function (err, done) {
            if (err) {
                //
                reject(err)
            } else {
                //
                resolve(done)
            }
        })
    })

}

// myWrite('akld.txt', 'test somehting')
//     .then(function (data) {
//         console.log('success in write', data)
//     })
//     .catch(function (err) {
//         console.log('error in write', err)
//     })

// myWrite("broadway.txt", 'hello', function (err, done) {
//     if (err) {
//         console.log('error in write ing ', err)
//     } else {
//         console.log('success ..')
//     }
// })


// fileOperation.a('node.js', 'lets learn nodejs', function (err, done) {
//     if (err) {
//         return console.log('error in write', err);
//     }
//     console.log('success in write >', done)

// })
